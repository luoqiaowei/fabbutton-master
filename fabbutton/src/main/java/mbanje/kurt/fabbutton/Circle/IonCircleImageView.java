/*
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Kurt Mbanje
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package mbanje.kurt.fabbutton.Circle;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.TransitionDrawable;
import android.net.Uri;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;

import mbanje.kurt.fabbutton.RoundImage;
import universalimageloader.core.DisplayImageOptions;
import universalimageloader.core.ImageLoader;
import universalimageloader.core.assist.FailReason;
import universalimageloader.core.assist.ImageSize;
import universalimageloader.core.listener.ImageLoadingListener;
import universalimageloader.core.listener.ImageLoadingProgressListener;


public class IonCircleImageView extends ImageView {

    public interface OnFabViewListener {
        public void onProgressVisibilityChanged(boolean visible);

        public void onProgressCompleted();

        public void onProgressChanged(float percent);
    }

    public static final int animationDuration = 200;
    private int centerY;
    private int centerX;
    private int viewRadius;
    private boolean progressVisible;
    private int circleRadius;
    private Paint circlePaint;//带阴影的实心背景层
    private OnFabViewListener fabViewListener;
    private final int ringAlpha = 75;
    private int ringRadius;
    private Paint ringPaint;//外面的环的背景层
    private float currentRingWidth;
    private float ringWidthRatio = 0.14f; //of a possible 1f;
    private Drawable drawables[] = new Drawable[2];
    private TransitionDrawable crossfader;

    private int ringWidth;
    private ObjectAnimator ringAnimator;

    float radiusY = 3.5f;
    float radiusX = 0f;
    float shadowRadius = 10f;
    int shadowTransparency = 100;
    private boolean showEndBitmap;
    private boolean showShadow = true;

    int currentProgress = 0;


    public IonCircleImageView(Context context) {
        super(context);
        init(context, null);
    }


    public IonCircleImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public IonCircleImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    public void setFabViewListener(OnFabViewListener fabViewListener) {
        this.fabViewListener = fabViewListener;
    }

    public void setShowShadow(boolean showShadow) {
        this.showShadow = showShadow;
    }

    private void init(Context context, AttributeSet attrs) {
        this.setFocusable(false);
        setClickable(true);
        circlePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        circlePaint.setStyle(Paint.Style.FILL);
        if (showShadow) {
            circlePaint.setShadowLayer(shadowRadius, radiusX, radiusY, Color.argb(shadowTransparency, 0, 0, 0));
        }

        ringPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        ringPaint.setStyle(Paint.Style.STROKE);
        setWillNotDraw(false);
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        final int pressedAnimationTime = animationDuration;
        ringAnimator = ObjectAnimator.ofFloat(this, "currentRingWidth", 0f, 0f);
        ringAnimator.setDuration(pressedAnimationTime);
        ringAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                if (fabViewListener != null) {
                    fabViewListener.onProgressVisibilityChanged(progressVisible);
                }
            }
        });

    }

    public void setShowEndBitmap(boolean showEndBitmap) {
        this.showEndBitmap = showEndBitmap;
    }


    final Runnable updater = new Runnable() {
        @Override
        public void run() {
            if (!progressVisible)
                showRing(true);
            fabViewListener.onProgressChanged((currentProgress++) * 0.01f);
            if (currentProgress <= 100) {
                postDelayed(updater, 50);
            }
        }
    };

    /**
     * sets the icon that will be shown on the fab icon
     *
     * @param resource the resource id of the icon
     * @param imageUrl the resource id of the endBitmapResource
     */
    public void setIcon(int resource, String imageUrl) {
        final Bitmap srcBitmap = BitmapFactory.decodeResource(getResources(), resource);
        setImageBitmap(srcBitmap);
        if (showEndBitmap && imageUrl != null && !imageUrl.isEmpty()) {
            //开启下载
//            postDelayed(updater, 1000);

            ImageLoader.getInstance().loadImage(imageUrl, (ImageSize) null, (DisplayImageOptions) null, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) {
                    showRing(true);
                    fabViewListener.onProgressVisibilityChanged(true);
                }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    showRing(false);
                    fabViewListener.onProgressVisibilityChanged(false);
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) {
                    drawables[0] = new BitmapDrawable(srcBitmap);
                    drawables[1] = new BitmapDrawable(bitmap);
                    crossfader = new TransitionDrawable(drawables);
                    crossfader.setCrossFadeEnabled(true);
                    setImageDrawable(crossfader);
                    fabViewListener.onProgressChanged(1.0f);
                }

                @Override
                public void onLoadingCancelled(String s, View view) {
                    showRing(false);
                    fabViewListener.onProgressVisibilityChanged(false);
                }
            }, new ImageLoadingProgressListener() {
                @Override
                public void onProgressUpdate(String imageUri, View view, int current, int total) {
                    float percent = current * 0.1f / (total * 0.1f);
                    if (percent < 1.0)
                        fabViewListener.onProgressChanged(percent);
                }
            });
        }

    }


    public void resetIcon() {
        crossfader.resetTransition();
    }


    /**
     * this sets the thickness of the ring as a fraction of the radius of the circle.
     *
     * @param ringWidthRatio the ratio 0-1
     */
    public void setRingWidthRatio(float ringWidthRatio) {
        this.ringWidthRatio = ringWidthRatio;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        final float ringR = ringRadius + currentRingWidth;
        canvas.drawCircle(centerX, centerY, ringR, ringPaint); // the outer ring
        canvas.drawCircle(centerX, centerY, circleRadius, circlePaint); //the actual circle
        super.onDraw(canvas);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        centerX = w / 2;
        centerY = h / 2;
        viewRadius = Math.min(w, h) / 2;
        ringWidth = Math.round((float) viewRadius * ringWidthRatio);
        circleRadius = viewRadius - ringWidth;
        ringPaint.setStrokeWidth(ringWidth);
        ringPaint.setAlpha(ringAlpha);
        //setStrokeWidth的宽度为ringWidth，所以外环的中心圆的半径应该减去StrokeWidth的一半
        ringRadius = circleRadius - ringWidth / 2;//此时的外环是被circlePaint遮住的
    }

    public float getCurrentRingWidth() {
        return currentRingWidth;
    }

    public void setCurrentRingWidth(float currentRingWidth) {
        this.currentRingWidth = currentRingWidth;
        this.invalidate();
    }



    /**
     * sets the color of the circle
     *
     * @param color the actual color to set to
     */
    public void setColor(int color) {
        circlePaint.setColor(color);
        ringPaint.setColor(color);
        ringPaint.setAlpha(ringAlpha);
        this.invalidate();
    }

    /**
     * whether to show the ring or not
     *
     * @param show set flag
     */
    public void showRing(boolean show) {
        progressVisible = show;
        if (show) {
            ringAnimator.setFloatValues(currentRingWidth, ringWidth);
        } else {
            ringAnimator.setFloatValues(ringWidth, 0f);
        }
        ringAnimator.start();
    }

    /**
     * this animates between the icon set in the imageview and the completed icon. does as crossfade animation
     *
     * @param show           set flag
     * @param hideOnComplete if true animate outside ring out after progress complete
     */
    public void showCompleted(boolean show, boolean hideOnComplete) {
        if (show) {
            if (crossfader != null)
                crossfader.startTransition(500);
        }
        if (hideOnComplete) {
            ObjectAnimator hideAnimator = ObjectAnimator.ofFloat(this, "currentRingWidth", 0f, 0f);
            hideAnimator.setFloatValues(1);
            hideAnimator.setDuration(animationDuration);
            hideAnimator.start();

        }
    }

    @Override
    public void setImageURI(Uri uri) {
        super.setImageURI(uri);
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        super.setImageDrawable(RoundImage.getRoundDrawable(drawable));
    }
}
