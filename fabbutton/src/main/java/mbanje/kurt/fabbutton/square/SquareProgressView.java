package mbanje.kurt.fabbutton.Square;

import android.animation.AnimatorSet;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.support.annotation.DimenRes;
import android.util.AttributeSet;
import android.view.View;

import java.text.DecimalFormat;

import mbanje.kurt.fabbutton.Util.CalculationUtil;
import mbanje.kurt.fabbutton.Util.FabUtil;
import mbanje.kurt.fabbutton.Util.PercentStyle;


public class SquareProgressView extends View implements FabUtil.OnFabValueCallback {

    private float progress = 0f;
    private Paint progressBarPaint;
    private Paint textPaint;
    private int progressColor = Color.TRANSPARENT;
    private float progressWidth = 0;
    private float strokewidth = 0;
    private Canvas canvas;
    private int viewRadius;
    private float ringWidthRatio = 0.14f; //of a possible 1f;
    private boolean showProgress = false,autostartanim = true;
    private int animDuration = 4000;
    private float actualProgress;
    private ValueAnimator progressAnimator;
    private AnimatorSet indeterminateAnimator;
    private PercentStyle percentSettings = new PercentStyle(Align.CENTER, 150,
            true);

    private IonSquareImageView.OnFabViewListener fabViewListener;

    public SquareProgressView(Context context) {
        super(context);
        init(null, 0);
    }

    public SquareProgressView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    public SquareProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public void init(AttributeSet attrs, int defStyle) {
        strokewidth = CalculationUtil.convertDpToPx(progressWidth, getContext());
        progressBarPaint = new Paint();
        progressBarPaint.setColor(progressColor);
        progressBarPaint.setStrokeWidth(strokewidth);
        progressBarPaint.setAntiAlias(true);
        progressBarPaint.setStyle(Style.STROKE);


        textPaint = new Paint();
        textPaint.setColor(getContext().getResources().getColor(
                android.R.color.black));
        textPaint.setAntiAlias(true);
        textPaint.setStyle(Style.STROKE);

        if(autostartanim) {
            startAnimation();
        }
    }

    public void setAutostartanim(boolean autostartanim) {
        this.autostartanim = autostartanim;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        this.canvas = canvas;
        super.onDraw(canvas);

        float scope = canvas.getWidth() + canvas.getHeight()
                + canvas.getHeight() + canvas.getWidth();
        float sweepPercent = isInEditMode() ? progress : actualProgress;
        float percent = (scope / 100) * sweepPercent;
        float halfOfTheImage = canvas.getWidth() / 2;

        if (showProgress) {
            drawPercent(percentSettings);
        }

        Path path = new Path();
        if (percent > halfOfTheImage) {
            paintFirstHalfOfTheTop(canvas);
            float second = percent - halfOfTheImage;

            if (second > canvas.getHeight()) {
                paintRightSide(canvas);
                float third = second - canvas.getHeight();

                if (third > canvas.getWidth()) {
                    paintBottomSide(canvas);
                    float forth = third - canvas.getWidth();

                    if (forth > canvas.getHeight()) {
                        paintLeftSide(canvas);
                        float fifth = forth - canvas.getHeight();

                        if (fifth == halfOfTheImage) {
                            paintSecondHalfOfTheTop(canvas);
                        } else {
                            path.moveTo(strokewidth, (strokewidth / 2));
                            path.lineTo(strokewidth + fifth, (strokewidth / 2));
                            canvas.drawPath(path, progressBarPaint);
                        }
                    } else {
                        path.moveTo((strokewidth / 2), canvas.getHeight()
                                - strokewidth);
                        path.lineTo((strokewidth / 2), canvas.getHeight()
                                - forth);
                        canvas.drawPath(path, progressBarPaint);
                    }

                } else {
                    path.moveTo(canvas.getWidth() - strokewidth,
                            canvas.getHeight() - (strokewidth / 2));
                    path.lineTo(canvas.getWidth() - third, canvas.getHeight()
                            - (strokewidth / 2));
                    canvas.drawPath(path, progressBarPaint);
                }
            } else {
                path.moveTo(canvas.getWidth() - (strokewidth / 2), strokewidth);
                path.lineTo(canvas.getWidth() - (strokewidth / 2), strokewidth
                        + second);
                canvas.drawPath(path, progressBarPaint);
            }

        } else {
            path.moveTo(halfOfTheImage, strokewidth / 2);
            path.lineTo(halfOfTheImage + percent, strokewidth / 2);
            canvas.drawPath(path, progressBarPaint);
        }

    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        viewRadius = Math.min(w, h) / 2;
        setRingWidth(-1, true);
    }

    public void paintFirstHalfOfTheTop(Canvas canvas) {
        Path path = new Path();
        path.moveTo(canvas.getWidth() / 2, strokewidth / 2);
        path.lineTo(canvas.getWidth() + strokewidth, strokewidth / 2);
        canvas.drawPath(path, progressBarPaint);
    }

    public void paintRightSide(Canvas canvas) {
        Path path = new Path();
        path.moveTo(canvas.getWidth() - (strokewidth / 2), strokewidth);
        path.lineTo(canvas.getWidth() - (strokewidth / 2), canvas.getHeight());
        canvas.drawPath(path, progressBarPaint);
    }

    public void paintBottomSide(Canvas canvas) {
        Path path = new Path();
        path.moveTo(canvas.getWidth() - strokewidth, canvas.getHeight()
                - (strokewidth / 2));
        path.lineTo(0, canvas.getHeight() - (strokewidth / 2));
        canvas.drawPath(path, progressBarPaint);
    }

    public void paintLeftSide(Canvas canvas) {
        Path path = new Path();
        path.moveTo((strokewidth / 2), canvas.getHeight() - strokewidth);
        path.lineTo((strokewidth / 2), 0);
        canvas.drawPath(path, progressBarPaint);
    }

    public void paintSecondHalfOfTheTop(Canvas canvas) {
        Path path = new Path();
        path.moveTo(strokewidth, (strokewidth / 2));
        path.lineTo(canvas.getWidth() / 2, (strokewidth / 2));
        canvas.drawPath(path, progressBarPaint);
    }

    public float getProgress() {
        return progress;
    }

    public void setProgress(float progress) {
        this.progress = progress;
        // Reset the determinate animation to approach the new progress
        if (progressAnimator != null && progressAnimator.isRunning()) {
            progressAnimator.cancel();
        }
        progressAnimator = FabUtil.createProgressAnimator(this, actualProgress, progress, this);
        progressAnimator.start();
        this.invalidate();
    }


    private void drawPercent(PercentStyle setting) {
        textPaint.setTextAlign(setting.getAlign());
        if (setting.getTextSize() == 0) {
            textPaint.setTextSize((canvas.getHeight() / 10) * 4);
        } else {
            textPaint.setTextSize(setting.getTextSize());
        }
        float sweepPercent = isInEditMode() ? progress : actualProgress;
        String percentString = new DecimalFormat("###").format(sweepPercent);
        if (setting.isPercentSign()) {
            percentString = percentString + percentSettings.getCustomText();
        }

        textPaint.setColor(percentSettings.getTextColor());

        canvas.drawText(
                percentString,
                canvas.getWidth() / 2,
                (int) ((canvas.getHeight() / 2) - ((textPaint.descent() + textPaint
                        .ascent()) / 2)), textPaint);
    }

    public boolean isShowProgress() {
        return showProgress;
    }

    public void setShowProgress(boolean showProgress) {
        this.showProgress = showProgress;
        this.invalidate();
    }

    public void setPercentStyle(PercentStyle percentSettings) {
        this.percentSettings = percentSettings;
        this.invalidate();
    }

    public PercentStyle getPercentStyle() {
        return percentSettings;
    }

    public int getProgressColor() {
        return progressColor;
    }

    public void setProgressColor(int progressColor) {
        this.progressColor = progressColor;
        this.progressBarPaint.setColor(progressColor);
        this.invalidate();
    }

    public IonSquareImageView.OnFabViewListener getFabViewListener() {
        return fabViewListener;
    }

    public void setFabViewListener(IonSquareImageView.OnFabViewListener fabViewListener) {
        this.fabViewListener = fabViewListener;
    }

    /**
     * Starts the progress bar animation.
     * (This is an alias of resetAnimation() so it does the same thing.)
     */
    public void startAnimation() {
        resetAnimation();
    }


    public void stopAnimation(boolean hideProgress) {
        if (progressAnimator != null && progressAnimator.isRunning()) {
            progressAnimator.cancel();
        }
        if (indeterminateAnimator != null && indeterminateAnimator.isRunning()) {
            indeterminateAnimator.cancel();
        }
        if (hideProgress) {
            setRingWidth(0, false);
        } else {
            setRingWidth(0, true);
        }
        invalidate();
    }

    /**
     * Resets the animation.
     */
    public void resetAnimation() {
        stopAnimation(false);
        // Determinate animation
        // The linear animation shown when progress is updated
        actualProgress = 0f;
        progressAnimator = FabUtil.createProgressAnimator(this, actualProgress, progress, this);
        progressAnimator.start();

    }


    @Override
    public void onIndeterminateValuesChanged(float indeterminateSweep, float indeterminateRotateOffset, float startAngle, float progress) {
        if(progress != -1){
            this.actualProgress = progress;
            if(Math.round(actualProgress) == 100 && fabViewListener != null){
                fabViewListener.onProgressCompleted();
            }
        }
    }

    public float getProgressWidth() {
        return progressWidth;
    }

    public void setProgressWidth(float progressWidth) {
        this.progressWidth = progressWidth;
        strokewidth = CalculationUtil.convertDpToPx(progressWidth, getContext());
        progressBarPaint.setStrokeWidth(strokewidth);
        this.invalidate();
    }

    public void setProgressWidth(@DimenRes int resID) {
        setProgressWidth(getResources().getDimension(resID));
    }

    public float getRingWidthRatio() {
        return ringWidthRatio;
    }

    public void setRingWidthRatio(float ringWidthRatio) {
        this.ringWidthRatio = ringWidthRatio;
    }

    public void setRingWidth(int width, boolean original) {
        if (original) {
            progressWidth = Math.round((float) viewRadius * ringWidthRatio);
        } else {
            progressWidth = width;
        }
        setProgressWidth(progressWidth);
    }

    public void setAnimDuration(int animDuration) {
        this.animDuration = animDuration;
    }
}
