package com.example.kurt.demo;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.View;

import universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import universalimageloader.core.DisplayImageOptions;
import universalimageloader.core.ImageLoader;
import universalimageloader.core.ImageLoaderConfiguration;
import universalimageloader.core.assist.QueueProcessingType;

import mbanje.kurt.fabbutton.Circle.IonFabImage;
import mbanje.kurt.fabbutton.Square.IonSquareFabImage;


public class MainActivity extends ActionBarActivity {
    String TAG = MainActivity.class.getSimpleName();
    IonSquareFabImage determinate;
    IonFabImage indeterminate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ImageLoaderConfiguration.Builder config = new ImageLoaderConfiguration.Builder(getApplicationContext());
        config.threadPriority(Thread.NORM_PRIORITY - 2);
        config.denyCacheImageMultipleSizesInMemory();
        config.diskCacheFileNameGenerator(new Md5FileNameGenerator());
        config.diskCacheSize(50 * 1024 * 1024); // 50 MiB
        config.tasksProcessingOrder(QueueProcessingType.LIFO);

        // Initialize ImageLoader with configuration.
        ImageLoader.getInstance().init(config.build());

        setContentView(R.layout.activity_main);

        determinate = (IonSquareFabImage) findViewById(R.id.determinate);
        indeterminate = (IonFabImage) findViewById(R.id.indeterminate);
        String imageUrl = "http://h.hiphotos.baidu.com/image/pic/item/574e9258d109b3deb38ea469cebf6c81800a4cf9.jpg";
        String image = "http://25.media.tumblr.com/af50758346e388e6e69f4c378c4f264f/tumblr_mzgzcdEDTL1st5lhmo1_1280.jpg";
        indeterminate.setImageUrl(imageUrl);
        determinate.setImageUrl(image);

        findViewById(R.id.button2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                determinate.resetIcon();
            }
        });



    }


}
